<?php
require "config.php";

if (isset($_POST['submit'])) {
    $old_namestudent = $_POST['old_namestudent'];
    $title = $_POST['title'];
    $npos = $_POST['npos'];
    $namegroup = $_POST['namegroup'];
    $namestudent = $_POST['namestudent'];
    $status = $_POST['status'];

    $msg = '';

    if (empty($old_namestudent)){
        $msg ="Введите ФИО для поиска! <br />";
    }else{
        $err4 = $pdo->prepare("SELECT Id FROM `Tasks` WHERE namestudent='$old_namestudent'");
        $err4->execute();

        if ($err4->rowCount() === 0){
            $msg = "Такого студента нет";
        }else{
            if (!empty($title)){
                $query = "UPDATE `Tasks`
					SET Title = '$title'
					WHERE namestudent='$old_namestudent'";

                $affected_rows += $pdo->exec($query);
            }
            if (!empty($npos)){
                $query = "UPDATE `Tasks`
					SET NPos = '$npos'
					WHERE namestudent='$old_namestudent'";

                $affected_rows += $pdo->exec($query);
            }
            if (!empty($namegroup)){
                $query = "UPDATE `Tasks`
					SET NameGroup = '$namegroup'
					WHERE namestudent='$old_namestudent'";

                $affected_rows += $pdo->exec($query);
            }
            if (!empty($namestudent)){
                $query = "UPDATE `Tasks`
					SET NameStudent = '$namestudent'
					WHERE namestudent='$old_namestudent'";

                $affected_rows += $pdo->exec($query);
            }
            if (!empty($status)){
                $query = "UPDATE `Tasks`
					SET Status = '$status'
					WHERE namestudent='$old_namestudent'";

                $affected_rows += $pdo->exec($query);
            }

            if ($affected_rows > 0) {
                $msg = "Задача отредактирована " . "Нажмите <a href='index.php'>назад</a>" . ", чтобы посмотреть";;
            } else {
                $msg = 'Попробуйте добавить еще раз';
            }
        }

    }
    $_SESSION['msg'] = $msg;
}

?>
<? include "site/header.php";?>
    <div id="content">
    <div id="main">
        <h1>Редактирование задачи</h1>
        <?= $_SESSION['msg']; ?>
        <? unset($_SESSION['msg']); ?>

        <form method='POST'>
            <em>Введите ФИО студента для поиска в базе</em><br>
            <input type='text' name='old_namestudent' value="">
            <br>
            Новый заголовок<br>
            <input type='text' name='title' value="">
            <br>
            Рейтинг<br>
            <input type='number' name='npos' value="">
            <br>
            Название группы<br>
            <input type='text' name='namegroup' value="">
            <br>
            ФИО студента<br>
            <input type='text' name='namestudent' value="">
            <br>
            Статус<br>
            <input type='text' name='status' value="">
            <br>
            <input style="float:left" type='submit' name='submit' value='Редактировать'>
        </form>
        <br><p>
            <a href="index.php">Назад</a>
        </p>
    </div>
<? include "site/sidebar.php";?>

<? include "site/footer.php";?>

<? unset($_SESSION['submit']); ?>