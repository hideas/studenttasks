<?php
require ("config.php");
require ("create_tables.php");

$sql1 = "SELECT * FROM `Tasks`";
$tasks = $pdo->query($sql1);

$sql2 = "SELECT * FROM `DetailTask`";
$detailtasks = $pdo->query($sql2);
?>



<? include "site/header.php";?>
<div id="content">
    <div id="main">
        <h1>Таблица задач</h1>
        <?
        // В случае ошибки с БД Tasks
        $err1 = $pdo->prepare('SELECT * FROM `Tasks`');
        $err1->execute();
        if ($err1->errorCode() !=0){
            echo nl2br("<pre>" . "PDOStatement::errorCode(): " . $err1->errorCode() . " </pre>");
        }
        ?>
        <table border="2">
            <tr>
                <td><b>Id</b></td>
                <td><b>Заголовок</b></td>
                <td><b>Рейтинг</b></td>
                <td><b>Название группы</b></td>
                <td><b>ФИО студента</b></td>
                <td><b>Статус</b></td>
            </tr>
            <? foreach ($tasks as $row) :?>
            <tr>
                <td><?=$row['Id'];?></td>
                <td><?=$row['Title'];?></td>
                <td><?=$row['NPos'];?></td>
                <td><?=$row['NameGroup'];?></td>
                <td><?=$row['NameStudent'];?></td>
                <td><?=$row['Status'];?></td>
            </tr>
        <? endforeach; ?>
        </table>
        <h1>Таблица описания задач</h1>
        <?
        // В случае ошибки с БД DetailTask
        $err2 = $pdo->prepare('SELECT * FROM `DetailTask`');
        $err2->execute();

        if ($err2->errorCode() !=0){
            echo nl2br("<pre>" . "PDOStatement::errorCode(): " . $err2->errorCode() . " </pre>");
        }
        ?>
        <table border="2">
            <tr>
                <td><b>Id</b></td>
                <td><b>Id задачи</b></td>
                <td><b>Картинка</b></td>
                <td><b>Описание</b></td>
            </tr>
            <? foreach ($detailtasks as $row) :?>
                <tr>
                    <td><?=$row['Id'];?></td>
                    <td><?=$row['IdTask'];?></td>
                    <td><?=$row['Img'];?></td>
                    <td><?=$row['About'];?></td>
                </tr>
            <? endforeach; ?>
        </table>
        <p>
            <a href="add_task.php">Добавить задачу</a>
        </p>
        <p>
            <a href="edit_task.php">Редактировать задачу</a>
        </p>
        <p>
            <a href="add_detail_task.php">Добавить описание задачи</a>
        </p>
    </div>
<? include "site/sidebar.php";?>

<? include "site/footer.php";?>