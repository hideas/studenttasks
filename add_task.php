<?php
require "config.php";

if (isset($_GET['submit'])) {
    $title = $_GET['title'];
    $npos = $_GET['npos'];
    $namegroup = $_GET['namegroup'];
    $namestudent = $_GET['namestudent'];
    $status = $_GET['status'];

    $msg = '';

    if (empty($title)) {
        $msg .= "Введите заголовок! <br />";
    }
    if (empty($namegroup)) {
        $msg .= "Введите навазние группы! <br />";
    }
    if (empty($namestudent)) {
        $msg .= "Введите ФИО студента! <br />";
    } else {
        $err3 = $pdo->prepare("SELECT Id FROM `Tasks` WHERE namestudent='$namestudent'");
        $err3->execute();

        if ($err3->rowCount() > 0){
            $msg = "Такая запись уже существует";
        }else{
            $sql3 = "INSERT INTO `Tasks` 
            (`Title`, `NPos`, `NameGroup`, `NameStudent`, `Status`)
            VALUES ('$title', '$npos', '$namegroup', '$namestudent', '$status')";

            $affected_rows = $pdo->exec($sql3);

            if ($affected_rows > 0) {
                $msg = "Задача добавлена " . "Нажмите <a href='index.php'>назад</a>" . ", чтобы посмотреть";;
            } else {
                $msg = 'Попробуйте добавить еще раз';
            }
        }
    }
    $_SESSION['msg'] = $msg;
}

?>
<? include "site/header.php"; ?>
<div id="content">
    <div id="main">
        <h1>Добавление задачи</h1>
        <?= $_SESSION['msg']; ?>
        <? unset($_SESSION['msg']); ?>
        <form method='GET'>
            Заголовок<br>
            <input type='text' name='title' value="">
            <br>
            Рейтинг<br>
            <input type='number' name='npos' value="">
            <br>
            Название группы<br>
            <input type='text' name='namegroup' value="">
            <br>
            ФИО студента<br>
            <input type='text' name='namestudent' value="">
            <br>
            Статус<br>
            <input type='text' name='status' value="">
            <br>
            <input style="float:left" type='submit' name='submit' value='Добавить'>
        </form>
        <br>
        <p>
            <a href="index.php">Назад</a>
        </p>
    </div>
    <? include "site/sidebar.php"; ?>

    <? include "site/footer.php"; ?>

    <? unset($_SESSION['submit']); ?>


