<?php

define("HOST","localhost");
define("USER","root");
define("PASSWORD","");
define("DB","studenttasks");

try {
    $pdo = new PDO('mysql:host=' . HOST . ';dbname=' . DB . ';charset=utf8', USER, PASSWORD);

    //$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo nl2br("Ошибка соединения с БД " . $e->getMessage());
    die();
}

?>