<?php
require "config.php";

// Создаем Tasks
$sql1 = "CREATE TABLE IF NOT EXISTS `Tasks`
        (`Id` INT NOT NULL AUTO_INCREMENT, 
        `Title` VARCHAR(40) NOT NULL,
        `NPos` INT NULL,
        `NameGroup` VARCHAR(10) NOT NULL,
        `NameStudent` VARCHAR(40) NOT NULL,
        `Status` VARCHAR(10) NULL DEFAULT 'Не задан',
        PRIMARY KEY (`Id`)
        ) ENGINE=InnoDb  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1";
$result1 = $pdo->exec($sql1);

// Создаем DetailTask
$sql2 = "CREATE TABLE IF NOT EXISTS `DetailTask`
        (`Id` INT NOT NULL AUTO_INCREMENT, 
        `IdTask` INT NOT NULL, 
        `Img` VARCHAR(255) NULL,
        `About` VARCHAR(10000) NULL DEFAULT 'Напишите что-нибудь',
        /*FOREIGN KEY (IdTask) REFERENCES `Tasks`(`Id`),*/
        PRIMARY KEY (`Id`)
        ) ENGINE=InnoDb  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1";
$result2 = $pdo->exec($sql2);
?>


